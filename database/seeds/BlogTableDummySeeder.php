<?php

use Illuminate\Database\Seeder;

class BlogTableDummySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Blog::class,100)->create();
    }
}

//DB::table('blog')->insert([
    //'title' =>    'tes',
    //'content' => 'tes',
    //'category' => 'tes'
//]);
